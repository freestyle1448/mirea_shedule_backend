package org.mirea.shedule.service;

import org.mirea.shedule.dto.LastUpdateOutDTO;
import org.mirea.shedule.dto.ScheduleGetDTO;
import org.mirea.shedule.models.Answer;
import org.mirea.shedule.models.Institute;
import org.mirea.shedule.models.Schedule;

import java.util.List;
import java.util.concurrent.CompletableFuture;


public interface UserService {
    CompletableFuture<Answer<Schedule>> getGroupScheduleByGroupName(ScheduleGetDTO scheduleGetDTO);

    CompletableFuture<Answer<List<Institute>>> getInstitutes();

    CompletableFuture<Answer<LastUpdateOutDTO>> getLastScheduleUpdate(ScheduleGetDTO scheduleGetDTO);
}
