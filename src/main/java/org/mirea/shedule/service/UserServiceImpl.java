package org.mirea.shedule.service;

import org.mirea.shedule.dto.LastUpdateOutDTO;
import org.mirea.shedule.dto.ScheduleGetDTO;
import org.mirea.shedule.models.Answer;
import org.mirea.shedule.models.Group;
import org.mirea.shedule.models.Institute;
import org.mirea.shedule.models.Schedule;
import org.mirea.shedule.repositories.GroupsRepository;
import org.mirea.shedule.repositories.InstitutesRepository;
import org.mirea.shedule.repositories.SchedulesRepository;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Async
@Service
public class UserServiceImpl implements UserService {
    private final SchedulesRepository schedulesRepository;
    private final InstitutesRepository institutesRepository;
    private final GroupsRepository groupsRepository;

    public UserServiceImpl(SchedulesRepository schedulesRepository, InstitutesRepository institutesRepository, GroupsRepository groupsRepository) {
        this.schedulesRepository = schedulesRepository;
        this.institutesRepository = institutesRepository;
        this.groupsRepository = groupsRepository;
    }

    @Override
    public CompletableFuture<Answer<Schedule>> getGroupScheduleByGroupName(ScheduleGetDTO scheduleGetDTO) {
        final Schedule byGroupAndInstitute = schedulesRepository.findByGroupAndInstitute(scheduleGetDTO.getGroup(), Integer.valueOf(scheduleGetDTO.getInstitute()));

        if (byGroupAndInstitute != null)
            return CompletableFuture.completedFuture(Answer.<Schedule>builder().success(1).response(byGroupAndInstitute).build());

        return CompletableFuture.completedFuture(Answer.<Schedule>builder().success(0).response(Schedule.builder().build()).build());
    }

    @Override
    public CompletableFuture<Answer<List<Institute>>> getInstitutes() {
        List<Institute> institutes = institutesRepository.findAll();
        List<Group> groups = groupsRepository.findAll();
        if (institutes.isEmpty())
            return CompletableFuture.completedFuture(Answer.<List<Institute>>builder().success(0).response(Collections.emptyList()).build());

        for (Institute institute : institutes) {
            for (Group group : groups)
                if (institute.getInstitute_num().equals(group.getInstitute_num()))
                    institute.addGroup(group.getGroup_name());
        }

        return CompletableFuture.completedFuture(Answer.<List<Institute>>builder().success(1).response(institutes).build());
    }

    @Override
    public CompletableFuture<Answer<LastUpdateOutDTO>> getLastScheduleUpdate(ScheduleGetDTO scheduleGetDTO) {
        final Schedule byGroupAndInstitute = schedulesRepository.findByGroupAndInstitute(scheduleGetDTO.getGroup(), Integer.valueOf(scheduleGetDTO.getInstitute()));

        if (byGroupAndInstitute != null)
            return CompletableFuture.completedFuture(Answer.<LastUpdateOutDTO>builder().success(1).response(LastUpdateOutDTO.builder()
                    .group(byGroupAndInstitute.getGroup())
                    .institute(byGroupAndInstitute.getInstitute())
                    .last_update(byGroupAndInstitute.getLast_update())
                    .build()).build());

        return CompletableFuture.completedFuture(Answer.<LastUpdateOutDTO>builder().success(0).response(LastUpdateOutDTO.builder().build()).build());
    }
}
