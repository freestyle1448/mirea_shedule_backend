package org.mirea.shedule.repositories;

import org.bson.types.ObjectId;
import org.mirea.shedule.models.Schedule;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SchedulesRepository extends MongoRepository<Schedule, ObjectId> {
    Schedule findByGroupAndInstitute(String groupName, Integer institute);
}
