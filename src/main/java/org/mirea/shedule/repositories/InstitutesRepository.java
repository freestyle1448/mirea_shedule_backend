package org.mirea.shedule.repositories;

import org.bson.types.ObjectId;
import org.mirea.shedule.models.Institute;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface InstitutesRepository extends MongoRepository<Institute, ObjectId> {
}
