package org.mirea.shedule.repositories;

import org.bson.types.ObjectId;
import org.mirea.shedule.models.Group;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface GroupsRepository extends MongoRepository<Group, ObjectId> {
}
