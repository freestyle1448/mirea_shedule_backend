package org.mirea.shedule.controllers;

import org.mirea.shedule.dto.LastUpdateOutDTO;
import org.mirea.shedule.dto.ScheduleGetDTO;
import org.mirea.shedule.models.Answer;
import org.mirea.shedule.models.Institute;
import org.mirea.shedule.models.Schedule;
import org.mirea.shedule.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;

@RestController
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/schedule/get")
    Callable<ResponseEntity<Answer<Schedule>>> getGroupSchedule(ScheduleGetDTO getDTO) {
        final CompletableFuture<Answer<Schedule>> schedule = userService.getGroupScheduleByGroupName(getDTO);

        return () -> {
            if (schedule.get().getSuccess() == 1)
                return ResponseEntity.ok(schedule.get());
            else
                return ResponseEntity.notFound().build();
        };
    }

    @PostMapping("/institutes/get")
    Callable<ResponseEntity<Answer<List<Institute>>>> getInstitutes() {
        final CompletableFuture<Answer<List<Institute>>> institutes = userService.getInstitutes();

        return () -> {
            if (institutes.get().getSuccess() == 1)
                return ResponseEntity.ok(institutes.get());
            else
                return ResponseEntity.notFound().build();
        };
    }

    @PostMapping("/schedule/get/last-update")
    Callable<ResponseEntity<Answer<LastUpdateOutDTO>>> getScheduleLastUpdate(ScheduleGetDTO getDTO) {
        final CompletableFuture<Answer<LastUpdateOutDTO>> lastUpdate = userService.getLastScheduleUpdate(getDTO);

        return () -> {
            if (lastUpdate.get().getSuccess() == 1)
                return ResponseEntity.ok(lastUpdate.get());
            else
                return ResponseEntity.notFound().build();
        };
    }
}
