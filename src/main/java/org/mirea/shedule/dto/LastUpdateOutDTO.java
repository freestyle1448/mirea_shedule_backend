package org.mirea.shedule.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LastUpdateOutDTO {
    private Integer institute = 0;
    private String group = "";
    private Long last_update = 0L;
}
