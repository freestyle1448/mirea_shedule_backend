package org.mirea.shedule.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Document(collection = "schedules")
public class Schedule {
    @Id
    private ObjectId id;
    private Integer institute = 0;
    private String group = "";
    private Long last_update = 0L;
    private List<List<List<Lesson>>> week;
}
