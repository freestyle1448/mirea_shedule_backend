package org.mirea.shedule.models;

final class LessonType {
    public static int LECTION = 0;
    public static int PRACTICE = 1;
    public static int LABORATORY = 2;
    public static int LECTION_AND_PRACTICE = 10;
    public static int LABORATORY_AND_PRACTICE = 12;
    public static int LABORATORY_AND_LECTION = 20;
    public static int SPORT = 100;
    public static int NONE = 999;
}
