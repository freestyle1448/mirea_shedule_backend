package org.mirea.shedule.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
class Lesson {
    private String name;
    private String teacher;
    private Integer type;
    private String room;
}
