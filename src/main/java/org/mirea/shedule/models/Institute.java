package org.mirea.shedule.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Document(collection = "institutes")
public class Institute {
    @Id
    private ObjectId id;
    private String institute_name = "";
    private Integer institute_num = 0;
    private List<String> groups = new ArrayList<>();


    public void addGroup(String groupName) {
        this.groups.add(groupName);
    }
}
